<?php
require_once('includes/connect.php');
$title = "Filter,search and find your dream car.";

include "header.php";
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<?php
if((isset($_POST['BudgetSelect'])) && (isset($_POST['VehicleTypeSelect']))){
$budget = $_POST['BudgetSelect'];
$car_type = $_POST['VehicleTypeSelect'];
$start=0;
$limit=12;
$id=1;

$union_query = "SELECT * FROM car_data WHERE car_price_range = '".$budget."' AND car_type = '".$car_type."' LIMIT ".$start.",".$limit."";
$union_result = $conn->query($union_query);
$rowcount=mysqli_num_rows($union_result);


?>
<script>
$( document ).ready(function() {
$('input:checkbox').removeAttr('checked');
$("input[type=checkbox][value=<?php echo $budget ?>]").prop('checked','true'); 
$("input[type=checkbox][value=<?php echo $car_type ?>]").prop('checked','true');
});
 
</script>
<?php
}
else { 
/*
if(isset($_GET['id']))
{
$select = 'SELECT *';
$from = ' FROM car_data';
$where = ' WHERE TRUE';
if(isset($_POST['pricerange'])){
$price_range = "'".implode("','",$_POST['pricerange'])."'";
echo $price_range;
$where .=" AND car_price_range IN(".$price_range.")";
}
if(isset($_POST['brand'])){
$brand = "'".implode("','",$_POST['brand'])."'";
$where .=" AND car_brand IN(".$brand.")";
}
if(isset($_POST['fueltype'])){
$fuel_type = "'".implode("','",$_POST['fueltype'])."'";
$where .=" AND car_fuel IN(".$fuel_type.")";
}

if(isset($_POST['mileage'])){
$mileage = "'".implode("','",$_POST['mileage'])."'";
$where .=" AND car_mileage_range IN(".$mileage.")";
}

if(isset($_POST['vehicletype'])){
$type = "'".implode("','",$_POST['vehicletype'])."'";
$where .=" AND car_type IN(".$type.")";
}

if(isset($_POST['transmission'])){
$transmission = "'".implode("','",$_POST['transmission'])."'";
$where .=" AND car_transmission IN(".$transmission.")";
}
if(isset($_POST['seating'])){
$seat = implode(",",$_POST['seating']);
$where .=" AND car_type IN(".$seat.")";
}

if(isset($_POST['displacement'])){
$displacement = "'".implode("','",$_POST['displacement'])."'";
$where .=" AND car_cc_range IN(".$displacement.")";
}

if(isset($_POST['power'])) $where .= "AND car_power='1'";
if(isset($_POST['abs'])) $where .= "AND car_abs='1'";
if(isset($_POST['airbag'])) $where .= "AND car_airbag='1'";
if(isset($_POST['keyless'])) $where .= "AND car_keyless='1'";
if(isset($_POST['remote'])) $where .= "AND car_remote='1'";
if(isset($_POST['alloywheel'])) $where .= "AND car_alloywheel='1'";
$where_simple = $where;
$start=0;
$limit=12;
$id=1;
$id=$_GET['id'];
$start=($id-1)*$limit;
$where.= " LIMIT ".$start.",".$limit."";

$union_query = $select . $from . $where;
$union_result = $conn->query($union_query);
$rowcount=mysqli_num_rows($union_result);
}
else{
*/

$select = 'SELECT *';
$from = ' FROM car_data';
$where = ' WHERE TRUE';

$where_simple = $where;
$start=0;
$limit=12;
$id=1;

$where.= " LIMIT ".$start.",".$limit."";

$union_query = $select . $from . $where;
$union_result = $conn->query($union_query);
$rowcount=mysqli_num_rows($union_result);




}
?>
<div class="loader">
<div class="spinner">
  <div class="bounce1"></div>
  <div class="bounce2"></div>
  <div class="bounce3"></div>
</div>
</div>
<div class="search_wrapper" style="display:block;clear:both;">

<div class="search-filter data">
<form id="form">
<div class="search-price-range filter">
<h2 class="filter-head">price range</h2>
<label><input type="checkbox" name="pricerange[]" id="pricerange1" value="1-5">1 to 5 lakhs</label><br>
<label><input type="checkbox" name="pricerange[]" id="pricerange2" value="5-10">5 to 10 lakhs</label><br>
<label><input type="checkbox" name="pricerange[]" id="pricerange3" value="10-20">10 to 20 lakhs</label><br>
<label><input type="checkbox" name="pricerange[]" id="pricerange4" value="20-50">20 to 50 lakhs</label><br>
<label><input type="checkbox" name="pricerange[]" id="pricerange5" value="50-100">50 lakhs to 1 crore</label><br>
<label><input type="checkbox" name="pricerange[]" id="pricerange6" value="100-200">above 1 crore</label><br>
</div>


<div class="search-brand filter">
<h2 class="filter-head">brand</h2>
<label><input type="checkbox" name="brand[]" id="brand1" value="toyota">Toyota</label><br>
<label><input type="checkbox" name="brand[]" id="brand2" value="maruti">Maruti</label><br>
<label><input type="checkbox" name="brand[]" id="brand3" value="hyundai">Hyundai</label><br>
<label><input type="checkbox" name="brand[]" id="brand4" value="chevrolet">Chevrolet</label><br>
<label><input type="checkbox" name="brand[]" id="brand5" value="volkswagen">Volksvagen</label><br>
<label><input type="checkbox" name="brand[]" id="brand6" value="bmw">BMW</label><br>
<label><input type="checkbox" name="brand[]" id="brand7" value="audi">Audi</label><br>
<label><input type="checkbox" name="brand[]" id="brand8" value="renault">Renault</label><br>
<label><input type="checkbox" name="brand[]" id="brand9" value="ford">Ford</label><br>
</div>


<div class="search-fuel-type filter">
<h2 class="filter-head">fuel type</h2>
<label><input type="checkbox" name="fueltype[]" id="fueltype1" value="diesel">Diesel</label><br>
<label><input type="checkbox" name="fueltype[]" id="fueltype3" value="petrol">Petrol</label><br>
<label><input type="checkbox" name="fueltype[]" id="fueltype4" value="lpg">LPG</label><br>
<label><input type="checkbox" name="fueltype[]" id="fueltype5" value="cng">CNG</label><br>
</div>


<div class="search-mileage filter">
<h2 class="filter-head">mileage</h2>
<label><input type="checkbox" name="mileage[]" id="mileage1" value="10-15">10 - 15 kmpl</label><br>
<label><input type="checkbox" name="mileage[]" id="mileage2" value="15-50">15kmpl and above</label><br>
</div>


<div class="search-vehicle-type filter">
<h2 class="filter-head">vehicle type</h2>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype1" value="hatchback">Hatchback</label><br>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype2" value="muv">MUV</label><br>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype3" value="suv">SUV</label><br>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype4" value="sedan">Sedan</label><br>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype5" value="minivan">Minivan</label><br>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype6" value="luxury">Luxury</label><br>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype7" value="convertible">Convertible</label><br>
<label><input type="checkbox" name="vehicletype[]" id="vehicletype8" value="hybrid">Hybrid</label><br>
</div>


<div class="search-transmission filter">
<h2 class="filter-head">transmission</h2>
<label><input type="checkbox" name="transmission[]" id="transmission1" value="m">Manual</label><br>
<label><input type="checkbox" name="transmission[]" id="transmission2" value="a">Automatic</label><br>
</div>


<div class="search-seating filter">
<h2 class="filter-head">seating</h2>
<label><input type="checkbox" name="seating[]" id="seating1" value="4">4 seater</label><br>
<label><input type="checkbox" name="seating[]" id="seating2" value="5">5 seater</label><br>
<label><input type="checkbox" name="seating[]" id="seating3" value="6">6 seater</label><br>
<label><input type="checkbox" name="seating[]" id="seating4" value="7">7 seater</label><br>
<label><input type="checkbox" name="seating[]" id="seating5" value="8">8 seater</label><br>
</div>


<div class="search-engine-displacement filter">
<h2 class="filter-head">engine displacement</h2>
<label><input type="checkbox" name="displacement[]" id="displacement1" value="0-800">under 800cc</label><br>
<label><input type="checkbox" name="displacement[]" id="displacement2" value="800-1000">800cc - 1000cc</label><br>
<label><input type="checkbox" name="displacement[]" id="displacement3" value="1000-2000">1000cc - 2000cc</label><br>
</div>


<div class="search-features filter">
<h2 class="filter-head">features</h2>
<label><input type="checkbox" name="power" id="features1" value="power">Power Steering</label><br>
<label><input type="checkbox" name="airbag" id="features2" value="airbag">Airbags</label><br>
<label><input type="checkbox" name="keyless" id="features3" value="keyless">Keyless Entry</label><br>
<label><input type="checkbox" name="alloywheel" id="features4" value="alloywheel">Alloy Wheels</label><br>
<label><input type="checkbox" name="abs" id="features5" value="abs">Anti Braking System</label><br>
<label><input type="checkbox" name="remote" id="features6" value="remote">Remote Trunk Opener</label><br>
</div>
</form>
</div>

<div class="car-data results-data data">

<?php if($rowcount > 0){
while($row = mysqli_fetch_assoc($union_result)): ?>
<div class="car-data-wrapper">
	<div class="car-data-img" style="margin-top:25px;">
	<img src="car_images/<?php echo $row['car_image']; ?>" width="250">
	</div><br>
		<div class="car-misc-data">
		
		<span style="padding:5px;font-weight:bold;font-size:18px;margin:5px;"><?php echo ucfirst($row['car_name']);?></span>
		<div style="clear:both;height:5px;"></div>
		<span style="padding:5px;"><?php echo ucfirst($row['car_brand']);?></span><br>
		</div>
	<a href="car_details.php?car_id=<?php echo $row['car_id'];?>"  class="more-details">more details</a>
</div>
		
		
<?php
 endwhile;
 
$rows=mysqli_num_rows(mysqli_query($conn,"select * from car_data".$where_simple.""));
$total=ceil($rows/$limit); ?>
<div style="clear:both;"></div>
<div class="paging">
<?php
if($rows > 12){


echo "<ul class='page'>";
if($id>1)
{
echo "<li><a href='".($id-1)."' class='button'><i class='fa fa-arrow-left' aria-hidden='true'></i></a></li>";
echo "<li><a href='".($id-1)."'>..</a></li>";
}

if(($id+6) < ($total-1)) $c = ($id+6);
else $c = $total;
for($i=$id;$i<=$c;$i++)
{

if($i==$id) { echo "<li class='current'>".$i."</li>"; }

else { echo "<li><a href='".$i."'>".$i."</a></li>"; }

}

if($id!=$total)
{
echo "<li><a href='".($id+1)."'>..</a></li>";
echo "<li><a href='".($id+1)."' class='button'><i class='fa fa-arrow-right' aria-hidden='true'></i></a></li>";
}
echo "</ul>";
}
echo "</div>";
 
  }
 else echo"<div class='no-data'>Sorry.No results found with your dream combination at the moment.</div>.";

?>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="assets/js/script.js"></script>



<?php
include "footer.php";
?>