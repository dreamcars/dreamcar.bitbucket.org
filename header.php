<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"></head>
    <link rel="icon" href="assets/images/svlogo2.jpg" type="image/gif" sizes="16x16">
<body>
<header>
<div class="logo">
	<h1><a href="index.php">dream car</a></h1>
</div>

<div class="menu">
	<ul>
	<li><a href="index.php" data-hover="home">home</a></li>
	<li><a href="search.php" data-hover="find your car">find your car</a></li>
	<li><a href="compare.php" data-hover="compare">compare</a></li>
	<li><a href="about.php" data-hover="about">about</a></li>
	<li><a href="feedback.php" data-hover="feedback">feedback</a></li>
	</ul>
</div>
</header>