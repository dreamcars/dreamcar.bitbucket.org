<?php
require_once('includes/connect.php');
$title = "Compare different cars of your choice!";

include "header.php";
$show = false;

$car_query="SELECT DISTINCT car_brand FROM car_data";
$car_query_result = $conn->query($car_query);


if(isset($_POST['compare'])){
$car_data1 = "SELECT * from car_data WHERE car_id=".$_POST['car_data1']."";
$car_result1 = $conn->query($car_data1);
$data1 = mysqli_fetch_assoc($car_result1);
$car_data2 = "SELECT * from car_data WHERE car_id=".$_POST['car_data2']."";
$car_result2 = $conn->query($car_data2);
$data2 = mysqli_fetch_assoc($car_result2);
$show = true;
}
?>


<div class="car-compare" style="min-height:470px;">
<div class="compare-form">
<form action="<?php echo $_SERVER['PHP_SELF'];?> " method="post">
<span class="select_car1">
<select name="car1" onChange="getcar1(this.value);" required>
<option value="" selected="selected">Select brand</option>
<?php while($row =  mysqli_fetch_assoc($car_query_result)){ ?>
<option value="<?php echo $row['car_brand']; ?>"><?php echo ucfirst($row['car_brand']);?></option>
<?php } ?>
</select>
<select name="car_data1" id="car_name1" class="car_name_brand">
<option value="" selected>Select car</option>
</select>
</span>
<span class="select_car2">
<select name="car2" onChange="getcar2(this.value);" required>
<option value="" selected="selected">Select brand</option>
<?php
mysqli_data_seek($car_query_result,0);
 while($row =  mysqli_fetch_assoc($car_query_result)){ ?>
<option value="<?php echo $row['car_brand']; ?>"><?php echo ucfirst($row['car_brand']);?></option>
<?php } ?>
</select>
<select name="car_data2" id="car_name2" class="car_name_brand">
<option value="">Select car</option>
</select>
</span>
<input type="submit" value="compare" name="compare">

</form>
</div>


<?php if($show): ?>
<div class="car-compare-details">
<table>
<tr><th>Parameters</th><th><?php echo $data1['car_name']; ?></th><th><?php echo $data2['car_name']; ?></th></tr>
<tr><td style="vertical-align:middle;">Preview</td><td><img src="car_images/<?php echo $data1['car_image']; ?>" width="150" ></td><td><img src="car_images/<?php echo $data2['car_image']; ?>" width="150"></td></tr>
<tr><td>Brand</td><td><?php echo ucfirst($data1['car_brand']); ?></td><td><?php echo ucfirst($data2['car_brand']); ?></td></tr>
<tr><td>Price</td><td><?php echo $data1['car_price']; ?> lakhs</td><td><?php echo $data2['car_price']; ?> lakhs</td></tr>
<tr><td>Mileage</td><td><?php echo $data1['car_mileage']; ?> kmpl</td><td><?php echo $data2['car_mileage']; ?> kmpl</td></tr>
<tr><td>Type</td><td><?php $ty = $data1['car_type'];if($ty == 'suv' || $ty == 'muv')  $ty=strtoupper($ty);else $ty=ucfirst($ty); echo $ty; ?></td><td><?php $ty = $data2['car_type'];if($ty == 'suv' || $ty == 'muv')  $ty=strtoupper($ty);else $ty=ucfirst($ty); echo $ty; ?></td></tr>
<tr><td>Fuel type</td><td><?php echo ucfirst($data1['car_fuel']); ?></td><td><?php echo ucfirst($data2['car_fuel']); ?></td></tr>
<tr><td>Transmission</td><td><?php if($data1['car_transmission'] == 'a') echo "Automatic";else echo "Manual"; ?></td>
<td><?php if($data2['car_transmission'] == 'a') echo "Automatic";else echo "Manual"; ?></td></tr>
<tr><td>Displacement</td><td><?php echo $data1['car_cc']; ?> cc</td><td><?php echo $data2['car_cc']; ?> cc</td></tr>
<tr><td>Seating</td><td><?php echo $data1['car_seat']; ?></td><td><?php echo $data2['car_seat']; ?></td></tr>

<tr><td>Power Steering</td>
<td>
<?php if($data1['car_power']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td>
<td>
<?php if($data2['car_power']) echo  '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td>
 </tr>
<tr><td>Airbags</td><td>
<?php if($data1['car_airbag']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td>
<td>
<?php if($data2['car_airbag']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td></tr>
<tr><td>ABS</td><td>
<?php if($data1['car_abs']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td>
<td>
<?php if($data2['car_abs']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td></tr>
<tr><td>Keyless Entry</td><td>
<?php if($data1['car_keyless']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td>
<td>
<?php if($data2['car_keyless']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td></tr>
<tr><td>Remote Trunk Opening</td><td>
<?php if($data1['car_remote']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td>
<td>
<?php if($data2['car_remote']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td></tr>
<tr><td>Alloy Wheels</td><td>
<?php if($data1['car_alloywheel']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td>
<td>
<?php if($data2['car_alloywheel']) echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
else echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
 ?>
 </td></tr>
</table>
</div>
<?php endif; ?>






</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>

function getcar1(val) {
	$.ajax({
	type: "POST",
	url: "get_car.php",
	data:'car_brand='+val,
	success: function(data){
		$("#car_name1").html(data);
	}
	});

}

function getcar2(val) {
	$.ajax({
	type: "POST",
	url: "get_car.php",
	data:'car_brand='+val,
	success: function(data){
		$("#car_name2").html(data);
	}
	});

}
</script>

<?php
include "footer.php"
?>