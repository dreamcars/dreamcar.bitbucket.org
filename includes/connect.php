<?php

error_reporting(0);

$db_host = 'localhost';
$db_user = 'thesparta_sv';
$db_name = 'thesparta_sv';

$conn = new mysqli($db_host, $db_user, $db_pass, $db_name);

if (mysqli_connect_errno()) {
	die('<h1>Could not connect to the database</h1><h2>Please try again after a few moments.</h2>');
}

?>