<?php
$title = "Find the car that suits you the best.";
 include "header.php"; ?>
<div class="home-wrapper" style="height:100%;min-height:490px;">
<div class="form-wrapper form-home-main-wrapper data">
<form id="form-home-main"  action="search.php" method="POST">
    <h1>Search your dream car</h1>
     <br>
     <br>
    <select name="BudgetSelect" id="BudgetSelect" class="custom-select" required>
            <option value="1-5">1 Lakh - 5 Lakh</option>
            <option value="5-10">5 Lakh - 10 Lakh</option>
            <option value="10-20">10 Lakh - 20 Lakh</option>
            <option value="20-50">20 Lakh - 50 Lakh</option>
            <option value="50-100">50 Lakh - 1 Crore</option>
            <option value="100-200">Above 1 Crore</option>
    </select><br>
    <select name="VehicleTypeSelect" id="VehicleTypeSelect" class="custom-select border" required>
            <option value="hatchback">Hatchback</option>
            <option value="sedan">Sedans</option>
            <option value="muv">MUV</option>
            <option value="suv">SUV</option>
            <option value="luxury">Luxury</option>
            <option value="hybrid">Hybrids</option>
            <option value="minivan">Minivans</option>
            <option value="convertible">Convertibles</option>
    </select><br>
    <input type="submit" value="search" name="search-submit" id="home_submit"/>
</form>
</div>

<div class="or">OR</div>

<div class="container">
	<div class="row">
		<div class="panel panel-default">
		<h1>Search car by name</h1>
			<div class="form-group">
				
				<input name="keysearch" value="" placeholder="Enter the name of the car" id="keysearch" type="text" class="form-control">
				
			</div>
			
			<div id="result"></div>
		</div>
	</div>
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
<?php include "footer.php"; ?>