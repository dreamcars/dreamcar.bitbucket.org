//live search

$(document).ready(function(){
	var req = null;
	$('#keysearch').on('keyup', function(){
		var key = $('#keysearch').val();
		if (key && key.length >= 2)
		{
			$('#keysearch').css('background', '#fff url(assets/images/loading.gif) no-repeat scroll 480px 8px');
			if (req)
				req.abort();
			req = $.ajax({
				url : 'livesearch.php',
				type : 'POST',
				cache : false,
				data : {
					keysearch : key,
				},
				success : function(data)
				{
					console.log(data)
					if (data)
					{
						$('#keysearch').css('background', '#fff url(assets/images/loading.gif) no-repeat scroll 480px 8px');
						$("#result").html(data).show();
					}
				}
			});
		}


		else
		{
			$('#result').css('display', 'none');
		}
 
	});
});



// checkbox remember
var checkboxValues = JSON.parse(localStorage.getItem('checkboxValues')) || {};
var $checkboxes = $(".search-filter :checkbox");

$checkboxes.on("change", function(){
  $checkboxes.each(function(){
    checkboxValues[this.id] = this.checked;
  });
  localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
});

$.each(checkboxValues, function(key, value) {
  $("#" + key).prop('checked', value);
});


$(document).ready(function(){

	var form = $('form');

	$('#form').change(function(e) {
	e.preventDefault();
		$.ajax({
			url:'filter.php',
			type: 'POST',
			cache: false,
			data: form.serialize(), 
			beforeSend: function(){
			
				$('.loader').css("display","block");
			},
			success: function(data){
				
				$('.loader').hide(100);
				$('.car-data').html(data);

			}
		});
	});
	
	$(document).on('click', ".paging a", function(e) {
	e.preventDefault();
	var id = $(this).attr("href");
	var data = $('#form').serializeArray();
	data.push({name: 'id', value: id});
	
//image full loader

$.fn.imagesLoaded = function () {

    var $imgs = this.find('img[src!=""]');

    if (!$imgs.length) {return $.Deferred().resolve().promise();}

    var dfds = [];  
    $imgs.each(function(){

        var dfd = $.Deferred();
        dfds.push(dfd);
        var img = new Image();
        img.onload = function(){dfd.resolve();}
        img.onerror = function(){dfd.resolve();}
        img.src = this.src;

    });

    return $.when.apply($,dfds);

}
	
		$.ajax({
			url:'filter.php',
			type: 'POST',
			cache: false,
			data: data, 
			beforeSend: function(){
			
				$('.loader').css("display","block");
			},
			success: function(data){
				$('.car-data').html(data).imagesLoaded().then(function(){
  
				$('.loader').hide(100);
        });

			}
		});
	});
});