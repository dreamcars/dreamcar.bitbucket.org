<?php
require_once("includes/connect.php");
$title = "Find more details of your favourite car.";
include "header.php";
$car_id = filter_var($_GET['car_id'], FILTER_VALIDATE_INT);
$car_query = "SELECT * FROM car_data WHERE car_id=".$car_id."";
$car_result = $conn->query($car_query);

?>
<div  style="min-height:270px;text-align:left">
<div class="car-details-full">
<a href="search.php" style="text-decoration:none;color:#fff;background:#09f;padding:4px;font-size:14px;border-radius:5px;float:left;clear:both;display:block;"><i class="fa fa-arrow-left" aria-hidden="true"></i>back to search</a><br><br>
<?php if($car_result){
$car = mysqli_fetch_assoc($car_result); ?>
<div class="car_details_data" style="text-align:center;">
<img src="car_images/<?php echo $car['car_image']; ?>" style="margin-top:80px;text-align:center;">
</div>
<div class="car_details_data">
<div style="margin-top:30px;">
<span style="font-weight:bold;color:#09f;font-size:23px;padding:2px;clear:both;display:block;"><?php echo $car['car_name']; ?></span>
<span style="font-weight:bold;padding:0px 8px;margin-top:8px !important;display:block;"><?php echo $car['car_brand']; ?></span><br><br>
<div style="text-align:left;">
<p>Price starts at <span class="strong"><?php echo $car['car_price']; ?> Lakhs</span></p>
<p>Vehicle type : <?php $ty = $car['car_type'];if($ty == 'suv' || $ty == 'muv')  $ty=strtoupper($ty);else $ty=ucfirst($ty); echo $ty; ?></p>
<p>Mileage : <?php echo $car['car_mileage']; ?> kmpl</p>
<p>Fuel type : <?php echo $car['car_fuel']; ?></p>
<p>Transmission : <?php ($car['car_transmission']=='a') ? $a="Automatic" : $a="Manual"; echo $a; ?></p>
<p>Engine displacement : <?php echo $car['car_cc']; ?> cc</p>
<p>Features : </p>
<p>
<ul style="margin-left:80px;line-height:20px;" class="feature_ul">
<?php if($car['car_power']) echo "<li>Power Steering</li>"; ?>
<?php if($car['car_abs']) echo "<li>ABS</li>"; ?>
<?php if($car['car_airbag']) echo "<li>Airbags</li>"; ?>
<?php if($car['car_remote']) echo "<li>Remote Trunk Opening</li>"; ?>
<?php if($car['car_alloywheel']) echo "<li>Alloy Wheel</li>"; ?>
<?php if($car['car_keyless']) echo "<li>Keyless Entry</li>"; ?>
</ul>
<p style="width:90%;margin-right:10px;text-align:justify;"><br><span style="font-size:14px;"> <?php echo $car['car_desc']; ?></span></p>
</div>
</div>
</div>
<?php 
}
else echo "<div style='line-height:470px;'>No data found</div>";

?>

</div>
</div>


<?php
include "footer.php";
?>